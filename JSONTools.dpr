program JSONTools;

uses
  Forms,
  FMain in 'FMain.pas' {frmMain},
  SynopseJSONTreeview in 'SynopseJSONTreeview.pas',
  SynopseJSONListview in 'SynopseJSONListview.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.HelpFile := ' ';
  Application.Title := 'JSON Tools';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
