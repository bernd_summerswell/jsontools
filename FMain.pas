unit FMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SynEdit, SynEditHighlighter, SynHighlighterJScript, SynHighlighterRuby,
  JvExControls, JvButton, JvTransparentButton,
  SynCommons, ExtCtrls, ComCtrls, JvDialogs, StdCtrls;

type
  TfrmMain = class(TForm)
    SynRubySyn1: TSynRubySyn;
    Panel1: TPanel;
    JvTransparentButton1: TJvTransparentButton;
    JvTransparentButton2: TJvTransparentButton;
    JvTransparentButton3: TJvTransparentButton;
    PageControl1: TPageControl;
    tsJSON: TTabSheet;
    SynEdit1: TSynEdit;
    tsTreeView: TTabSheet;
    TreeView1: TTreeView;
    tsGridView: TTabSheet;
    ListView1: TListView;
    JvTransparentButton4: TJvTransparentButton;
    Panel2: TPanel;
    JvTransparentButton5: TJvTransparentButton;
    JvTransparentButton6: TJvTransparentButton;
    Panel3: TPanel;
    JvTransparentButton7: TJvTransparentButton;
    JvSaveDialog1: TJvSaveDialog;
    Label1: TLabel;
    procedure JvTransparentButton1Click(Sender: TObject);
    procedure JvTransparentButton2Click(Sender: TObject);
    procedure JvTransparentButton3Click(Sender: TObject);
    procedure JvTransparentButton4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvTransparentButton5Click(Sender: TObject);
    procedure JvTransparentButton6Click(Sender: TObject);
    procedure JvTransparentButton7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function IsValidJSON( const AJSON: RawUTF8 ): Boolean;
  end;

var
  frmMain: TfrmMain;

implementation

uses
  Clipbrd,
  SynopseJSONTreeview,
  SynopseJSONListview;

{$R *.dfm}


{-------------------------------------------------------------------------------
  Procedure: TfrmMain.FormCreate
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.FormCreate(Sender: TObject);
begin
  SynEdit1.Lines.Text := '';
end;


{-------------------------------------------------------------------------------
  Procedure: TfrmMain.IsValidJSON
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: const AJSON: string
  Result:    Boolean
-------------------------------------------------------------------------------}
function TfrmMain.IsValidJSON(const AJSON: RawUTF8 ): Boolean;
begin
end;

{-------------------------------------------------------------------------------
  Procedure: TfrmMain.JvTransparentButton1Click
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.JvTransparentButton1Click(Sender: TObject);
begin
  SynEdit1.Lines.Text := Clipboard.AsText;
end;

{-------------------------------------------------------------------------------
  Procedure: TfrmMain.JvTransparentButton2Click
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.JvTransparentButton2Click(Sender: TObject);
var
  LJSONFormatted : RawUTF8;
begin
  LJSONFormatted := JSONReformat( SynEdit1.Lines.Text, jsonHumanReadable );
  if ( LJSONFormatted <> 'null' ) then
    if ( IsStringJSON( @LJSONFormatted ) ) then begin
      SynEdit1.Lines.Text := LJSONFormatted;
      Exit;
    end;
  ShowMessage( 'This doesn''t seem to be valid JSON.' )
end;

{-------------------------------------------------------------------------------
  Procedure: TfrmMain.JvTransparentButton3Click
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.JvTransparentButton3Click(Sender: TObject);
begin
  TreeView1.Items.Clear();
  JSONToTreeview( _JsonFast( SynEdit1.Lines.Text ), TreeView1 );
  tsTreeView.TabVisible := True;
  PageControl1.ActivePage := tsTreeView;
end;

{-------------------------------------------------------------------------------
  Procedure: TfrmMain.JvTransparentButton4Click
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.JvTransparentButton4Click(Sender: TObject);
begin
  ListView1.Columns.Clear();
  ListView1.Clear();
  JSONToListview( _JsonFast( SynEdit1.Lines.Text ), ListView1 );
  tsGridView.TabVisible := True;
  PageControl1.ActivePage := tsGridView;
end;

{-------------------------------------------------------------------------------
  Procedure: TfrmMain.JvTransparentButton5Click
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.JvTransparentButton5Click(Sender: TObject);
begin
  TreeView1.FullExpand();
end;

{-------------------------------------------------------------------------------
  Procedure: TfrmMain.JvTransparentButton6Click
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.JvTransparentButton6Click(Sender: TObject);
begin
  TreeView1.FullCollapse();
end;

{-------------------------------------------------------------------------------
  Procedure: TfrmMain.JvTransparentButton7Click
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.JvTransparentButton7Click(Sender: TObject);
begin
  if ( JvSaveDialog1.Execute() ) then
    SynEdit1.Lines.SaveToFile( JvSaveDialog1.FileName );
end;

end.
