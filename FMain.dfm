object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'JSONTools'
  ClientHeight = 718
  ClientWidth = 858
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 858
    Height = 45
    Align = alTop
    TabOrder = 0
    object JvTransparentButton1: TJvTransparentButton
      Left = 81
      Top = 2
      Width = 193
      Height = 40
      Caption = 'Paste From Clipboard'
      Color = 13335348
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Helvetica'
      Font.Style = []
      HotTrackFont.Charset = DEFAULT_CHARSET
      HotTrackFont.Color = clWindowText
      HotTrackFont.Height = -16
      HotTrackFont.Name = 'Helvetica'
      HotTrackFont.Style = []
      FrameStyle = fsIndent
      ParentFont = False
      Spacing = 5
      Transparent = False
      OnClick = JvTransparentButton1Click
    end
    object JvTransparentButton2: TJvTransparentButton
      Left = 274
      Top = 2
      Width = 193
      Height = 40
      Caption = 'Format JSON'
      Color = 13335348
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Helvetica'
      Font.Style = []
      HotTrackFont.Charset = DEFAULT_CHARSET
      HotTrackFont.Color = clWindowText
      HotTrackFont.Height = -16
      HotTrackFont.Name = 'Helvetica'
      HotTrackFont.Style = []
      FrameStyle = fsIndent
      ParentFont = False
      Spacing = 5
      Transparent = False
      OnClick = JvTransparentButton2Click
    end
    object JvTransparentButton3: TJvTransparentButton
      Left = 468
      Top = 2
      Width = 193
      Height = 40
      Caption = 'View Treeview'
      Color = 13335348
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Helvetica'
      Font.Style = []
      HotTrackFont.Charset = DEFAULT_CHARSET
      HotTrackFont.Color = clWindowText
      HotTrackFont.Height = -16
      HotTrackFont.Name = 'Helvetica'
      HotTrackFont.Style = []
      FrameStyle = fsIndent
      ParentFont = False
      Spacing = 5
      Transparent = False
      OnClick = JvTransparentButton3Click
    end
    object JvTransparentButton4: TJvTransparentButton
      Left = 662
      Top = 2
      Width = 193
      Height = 40
      Caption = 'View Gridview'
      Color = 13335348
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Helvetica'
      Font.Style = []
      HotTrackFont.Charset = DEFAULT_CHARSET
      HotTrackFont.Color = clWindowText
      HotTrackFont.Height = -16
      HotTrackFont.Name = 'Helvetica'
      HotTrackFont.Style = []
      FrameStyle = fsIndent
      ParentFont = False
      Spacing = 5
      Transparent = False
      OnClick = JvTransparentButton4Click
    end
    object Label1: TLabel
      Left = 4
      Top = 2
      Width = 76
      Height = 40
      Caption = '{jt}'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -32
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 45
    Width = 858
    Height = 673
    ActivePage = tsJSON
    Align = alClient
    TabOrder = 1
    ExplicitTop = 57
    ExplicitWidth = 794
    ExplicitHeight = 661
    object tsJSON: TTabSheet
      Caption = 'RAW JSON'
      ExplicitWidth = 285
      ExplicitHeight = 165
      object SynEdit1: TSynEdit
        Left = 0
        Top = 33
        Width = 850
        Height = 612
        Align = alClient
        ActiveLineColor = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        TabOrder = 0
        Gutter.Font.Charset = DEFAULT_CHARSET
        Gutter.Font.Color = clWindowText
        Gutter.Font.Height = -11
        Gutter.Font.Name = 'Courier New'
        Gutter.Font.Style = []
        Highlighter = SynRubySyn1
        FontSmoothing = fsmClearType
        ExplicitTop = 35
        ExplicitHeight = 600
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 850
        Height = 33
        Align = alTop
        TabOrder = 1
        ExplicitWidth = 786
        object JvTransparentButton7: TJvTransparentButton
          Left = 4
          Top = 5
          Width = 92
          Height = 24
          Caption = 'Save to File'
          Color = 13335348
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Helvetica'
          Font.Style = []
          HotTrackFont.Charset = DEFAULT_CHARSET
          HotTrackFont.Color = clWindowText
          HotTrackFont.Height = -11
          HotTrackFont.Name = 'Helvetica'
          HotTrackFont.Style = []
          FrameStyle = fsIndent
          ParentFont = False
          Spacing = 5
          Transparent = False
          OnClick = JvTransparentButton7Click
        end
      end
    end
    object tsTreeView: TTabSheet
      Caption = 'Tree View'
      ImageIndex = 1
      TabVisible = False
      ExplicitWidth = 561
      ExplicitHeight = 365
      object TreeView1: TTreeView
        Left = 0
        Top = 34
        Width = 850
        Height = 611
        Align = alClient
        Indent = 19
        TabOrder = 0
        ExplicitTop = 552
        ExplicitWidth = 794
        ExplicitHeight = 166
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 850
        Height = 34
        Align = alTop
        TabOrder = 1
        ExplicitWidth = 786
        object JvTransparentButton5: TJvTransparentButton
          Left = 4
          Top = 5
          Width = 92
          Height = 24
          Caption = 'Expand All'
          Color = 13335348
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Helvetica'
          Font.Style = []
          HotTrackFont.Charset = DEFAULT_CHARSET
          HotTrackFont.Color = clWindowText
          HotTrackFont.Height = -11
          HotTrackFont.Name = 'Helvetica'
          HotTrackFont.Style = []
          FrameStyle = fsIndent
          ParentFont = False
          Spacing = 5
          Transparent = False
          OnClick = JvTransparentButton5Click
        end
        object JvTransparentButton6: TJvTransparentButton
          Left = 101
          Top = 5
          Width = 92
          Height = 24
          Caption = 'Collapse All'
          Color = 13335348
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Helvetica'
          Font.Style = []
          HotTrackFont.Charset = DEFAULT_CHARSET
          HotTrackFont.Color = clWindowText
          HotTrackFont.Height = -11
          HotTrackFont.Name = 'Helvetica'
          HotTrackFont.Style = []
          FrameStyle = fsIndent
          ParentFont = False
          Spacing = 5
          Transparent = False
          OnClick = JvTransparentButton6Click
        end
      end
    end
    object tsGridView: TTabSheet
      Caption = 'Grid View'
      ImageIndex = 2
      TabVisible = False
      ExplicitWidth = 786
      ExplicitHeight = 633
      object ListView1: TListView
        Left = 0
        Top = 0
        Width = 850
        Height = 645
        Align = alClient
        Columns = <>
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
        ExplicitLeft = 176
        ExplicitTop = 96
        ExplicitWidth = 250
        ExplicitHeight = 150
      end
    end
  end
  object SynRubySyn1: TSynRubySyn
    Options.AutoDetectEnabled = False
    Options.AutoDetectLineLimit = 0
    Options.Visible = False
    Left = 768
    Top = 120
  end
  object JvSaveDialog1: TJvSaveDialog
    DefaultExt = '*.json'
    FileName = '*.json'
    Filter = 'JSON Files|*.json'
    Height = 0
    Width = 0
    Left = 768
    Top = 168
  end
end
