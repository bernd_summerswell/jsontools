unit SynopseJSONTreeview;

interface

uses
  SynCommons,
  ComCtrls;

procedure JSONToTreeview( const AJSON: Variant; const ATreeView: TTreeView );
procedure ProcessNode( const ATreeView: TTreeView; const AParentNode: TTreeNode; const AJSONNode: TDocVariantData );
procedure ProcessArray( const ATreeView: TTreeView; const AParentNode: TTreeNode; const AJSONNode: TDocVariantData );


implementation

uses
  Variants;


{-------------------------------------------------------------------------------
  Procedure: ProcessNode
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: const ATreeView: TTreeView; const AParentNode: TTreeNode; const AJSONNode: TDocVariantData
  Result:    None
-------------------------------------------------------------------------------}
procedure ProcessNode( const ATreeView: TTreeView; const AParentNode: TTreeNode; const AJSONNode: TDocVariantData );
var
  LItemIndex : Integer;
  LCurrent   : TTreeNode;
begin
  for LItemIndex := 0 to AJSONNode.Count - 1 do begin

    if ( VarType( AJSONNode.Values[LItemIndex] ) = DocVariantType.VarType ) then begin
      if ( DocVariantData( AJSONNode.Values[LItemIndex] ).Kind = dvArray ) then begin
        LCurrent := ATreeView.Items.AddChild( AParentNode, FormatUTF8( '% : [%]', [ AJSONNode.Names[LItemIndex], DocVariantData( AJSONNode.Values[LItemIndex] ).Count ] ) );
        ProcessArray( ATreeView, LCurrent, DocVariantData( AJSONNode.Values[LItemIndex] )^ );
      end
      else if ( DocVariantData( AJSONNode.Values[LItemIndex] ).Kind = dvObject ) then begin
        LCurrent := ATreeView.Items.AddChild( AParentNode, AJSONNode.Names[LItemIndex] + ' : {}');
        ProcessNode( ATreeView, LCurrent, DocVariantData( AJSONNode.Values[LItemIndex] )^ );
      end
    end
    else
      ATreeView.Items.AddChild( AParentNode,
                                FormatUTF8( '% : % (%)', [ AJSONNode.Names[LItemIndex],
                                                           VarToStr( AJSONNode.Values[LItemIndex] ),
                                                           VarTypeAsText( VarType(  AJSONNode.Values[LItemIndex]  ) ) ] ) );
  end;
end;


{-------------------------------------------------------------------------------
  Procedure: ProcessArray
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: const ATreeView: TTreeView; const AParentNode: TTreeNode; const AJSONNode: TDocVariantData
  Result:    None
-------------------------------------------------------------------------------}
procedure ProcessArray( const ATreeView: TTreeView; const AParentNode: TTreeNode; const AJSONNode: TDocVariantData );
var
  LIndex    : Integer;
  LCurrent  : TTreeNode;
begin
  for LIndex := 0 to AJSONNode.Count - 1 do begin

    if ( VarType( AJSONNode.Values[LIndex] ) = DocVariantType.VarType ) then begin
      if ( DocVariantData( AJSONNode.Values[LIndex] ).Kind = dvObject ) then  begin
        LCurrent := ATreeView.Items.AddChild( AParentNode, FormatUTF8( '[%] {} ', [ LIndex ] ) );
        ProcessNode( ATreeView, LCurrent, DocVariantData( AJSONNode.Values[LIndex] )^ );
      end;
    end
    else begin

      LCurrent := ATreeView.Items.AddChild( AParentNode, FormatUTF8( '[%] % (%) ', [ LIndex, VarToStr( AJSONNode.Values[LIndex] ), VarTypeAsText( VarType(  AJSONNode.Values[LIndex])) ] ) );

    end;
  end;
end;

{-------------------------------------------------------------------------------
  Procedure: JSONToTreeview
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: const AJSON: Variant; const ATreeView: TTreeView
  Result:    None
-------------------------------------------------------------------------------}
procedure JSONToTreeview( const AJSON: Variant; const ATreeView: TTreeView );
begin
  if ( VarType( AJSON ) = varEmpty ) then Exit;

  ATreeView.Items.BeginUpdate();
  try
    ATreeView.Items.Clear();
    if ( DocVariantData( AJSON ).Kind = dvArray ) then
      ProcessArray( ATreeView, nil, DocVariantData( AJSON )^ )
    else if ( DocVariantData( AJSON ).Kind = dvObject ) then
      ProcessNode( ATreeView, nil, DocvariantData( AJSON )^ );

  finally
    ATreeView.Items.EndUpdate();
  end;
end;

end.
