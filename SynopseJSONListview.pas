unit SynopseJSONListview;

interface

uses
  SynCommons,
  ComCtrls;

procedure JSONToListview( const AJSON: Variant; const AListView: TListView );

implementation

uses
  Variants;


{-------------------------------------------------------------------------------
  Procedure: JSONToListview
  Author:    bvonfintel
  DateTime:  2014.12.17
  Arguments: const AJSON: Variant; const AListView: TListView
  Result:    None
-------------------------------------------------------------------------------}
procedure JSONToListview( const AJSON: Variant; const AListView: TListView );
var
  LIndex      : Integer;
  LItemIndex  : Integer;
  LColumn     : TListColumn;
  LNew        : TListItem;
begin
  if ( VarType( AJSON ) = varEmpty ) then Exit;
  if ( DocVariantData( AJSON ).Count = 0 ) then Exit;

  AListView.Items.BeginUpdate();
  try
    AListView.Columns.Add.Caption := '#';

    for LIndex := 0 to DocVariantData( AJSON._(0) ).Count - 1 do begin
      LColumn := AListView.Columns.Add();
      LColumn.Caption := DocVariantData( AJSON._(0) ).Names[LIndex];
      LColumn.Width := 100;
      LColumn.AutoSize := True;
    end;

    for LIndex := 0 to AJSON._Count - 1 do begin
      LNew := AListView.Items.Add();
      LNew.Caption := IntToString( LIndex );

      for LItemIndex := 0 to DocVariantData( AJSON._(LIndex) ).Count - 1 do begin
        LNew.SubItems.Add( VarToStr( DocVariantData( AJSON._(LIndex) ).Values[LItemIndex] ) );
      end;
    end;

    for LIndex := 0 to AListView.Columns.Count - 1 do
      AListView.Column[LIndex].AutoSize := True;

  finally
    AListView.Items.EndUpdate();
  end;
end;

end.
